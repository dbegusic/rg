from pyglet.gl import *
import numpy as np
from pyglet.window import key
import ctypes
timer = 0

def rotation(s, e):
        os = np.cross(s, e)
        se = np.dot(s, e)
        s_norm = np.linalg.norm(s)
        e_norm = np.linalg.norm(e)
        angle = np.rad2deg(np.arccos(se / (s_norm * e_norm)))
        return os, angle
        
def rotationDCM(tangent,d2):
    tangent = tangent/np.linalg.norm(tangent)
    d2 = d2/np.linalg.norm(d2)
    w = tangent
    u = np.cross(tangent,d2)
    v = np.cross(w,u)
    tmp = []
    tmp.append([w[0],u[0],v[0],0])
    tmp.append([w[1],u[1],v[1],0])
    tmp.append([w[2],u[2],v[2],0])
    tmp.append([0,0,0,1])
    R = np.array(tmp)
    return np.linalg.inv(R)

class Object:
    vertices = []
    polygons = []
    batch = pyglet.graphics.Batch()
    def __init__(self,filepath):
        data = open(filepath, 'r')
        for line in data:
            if line[0]=='v':
                tmp = line.split(" ")
                self.vertices.append(list(map(float,tmp[1:4])))
            if line[0]=='f':
                tmp = line.split(" ")
                self.polygons.append(list(map(int, tmp[1:4])))
        self.setBatch()
    def setBatch(self):
        for polygon in self.polygons:
            v1 = self.vertices[polygon[0]-1]
            v2 = self.vertices[polygon[1]-1]
            v3 = self.vertices[polygon[2]-1]
            self.batch.add(3,GL_TRIANGLES,None,('v3f', [v1[0],v1[1],v1[2],v2[0],v2[1],v2[2],v3[0],v3[1],v3[2]]))

class BSpline:
    vertices = []
    polygons = []
    points = []
    tangents = []
    d2s = []
    resolution = 20
    batch = pyglet.graphics.Batch()

    def __init__(self,filepath):
        data = open(filepath, 'r')
        for line in data:
            if line[0]=='v':
                tmp = line.split(" ")
                self.vertices.append(list(map(float,tmp[1:4])))
            if line[0]=='f':
                tmp = line.split(" ")
                self.polygons.append(list(map(int, tmp[1:4])))
        self.getSpline()
        self.setBatch()

    def derivate2(self, index, t):           
        T2 = [2*t, 1]
        B2 = 1/2 * np.array([[-1, 3, -3, 1],[2, -4, 2, 0]])
        R = np.array([self.vertices[index-1],self.vertices[index],self.vertices[index+1],self.vertices[index+2]])
        TB2 = np.dot(T2, B2)
        TBR2 = np.dot(TB2, R)
        
        return TBR2

    def getSegment(self,index,t):
        T = np.array([t**3, t**2, t, 1])
        B = 1/6 * np.array([[-1, 3, -3, 1],[3, -6, 3, 0],  [-3, 0, 3, 0],  [1, 4, 1, 0]])
        R = np.array([self.vertices[index-1],self.vertices[index],self.vertices[index+1],self.vertices[index+2]])
        TB = np.dot(T,B)
        TBR = np.dot(TB, R)
        T1 = [t**2, t, 1]
        B1 = 1/2 * np.array([[-1, 3, -3, 1],[2, -4, 2, 0],[-1, 0, 1, 0]])
        TB1 = np.dot(T1, B1)
        TBR1 = np.dot(TB1, R)
        return TBR,TBR1

    def getSpline(self):
        for index in range(1, len(self.vertices)-3+1):
            for t in np.linspace(0,1, self.resolution):
                points, tangent =  self.getSegment(index, t)
                self.points.append(points)
                self.tangents.append(tangent)
                self.d2s.append(self.derivate2(index,t))

    def tangent(self,segment,index):
        scale = 1
        return [segment[0],segment[1],segment[2],segment[0]+self.tangents[index][0]/scale,segment[1]+self.tangents[index][1]/scale,segment[2]+self.tangents[index][2]/scale]

    def setBatch(self):
        coordinates = []
        tangents = []
        for i in range(0,len(self.points)):
            for coordinate in self.tangent(self.points[i],i):
                tangents.append(coordinate)
            for coordinate in self.points[i]:
                coordinates.append(coordinate)
        self.batch.add(len(self.points),GL_LINE_STRIP,None,('v3f',coordinates))
        for i in range(0,int(len(tangents)/6)):
            self.batch.add(2,GL_LINES,None,('v3f',tangents[6*i:6*i+6]),('c3d',[0.2, 0.3, 0.4, 0.5, 0.6, 0.7]))

obj = Object("teapot.obj")
spline = BSpline("bspline.obj")

pos = [0, 0, -40]
rot_y = 30
config = Config(sample_buffers=1, samples=8)
window = pyglet.window.Window(height=700, width=700, config=config)

@window.event
def on_draw():
    global pos_z, rot_y
    global timer
    global asdf
    window.clear()

    glMatrixMode(GL_PROJECTION)
    glLoadIdentity()
    gluPerspective(90, 1, 1, 100)

    glMatrixMode(GL_MODELVIEW)
    glLoadIdentity()

    glTranslatef(pos[0],pos[1],pos[2])
    glRotatef(rot_y, 0, 1, 0)

    glPushMatrix()
    segment_point = spline.points[timer]
    
    #glTranslatef(segment_point[0],segment_point[1],segment_point[2])
    #rot_axis, rot_angle = rotation([1,0,1], spline.tangents[timer])
    #glRotatef(rot_angle, rot_axis[0], rot_axis[1], rot_axis[2])
    
    #ISPIS MATRICE - ROTACIJA
    #m = (GLfloat * 16)()
    #glGetFloatv(GL_MODELVIEW_MATRIX, m);
    #print(m[0:16])
    
    #glPopMatrix()
    
    glTranslatef(segment_point[0],segment_point[1],segment_point[2])
    R_inv = rotationDCM(spline.tangents[timer],spline.d2s[timer])
    R_inv_c = []
    for i in [0,1,2,3]:
        for j in [0,1,2,3]:
            R_inv_c.append(R_inv[i][j])
    R_inv_c = (ctypes.c_float * len(R_inv_c))(*R_inv_c)
      
    #glPushMatrix()
    
    glMultMatrixf(R_inv_c)
    
    #ISPIS MATRICE - ROTACIJA DCM
    #m = (GLfloat * 16)()
    #glGetFloatv(GL_MODELVIEW_MATRIX, m);
    #print(m[0:16])
    
    
    obj.batch.draw()
    glPopMatrix()
    
    glPushMatrix()
    spline.batch.draw()
    glPopMatrix()
    
    glFlush()

def update(dt):
    global timer
    timer = timer + 1
    if timer >= len(spline.points):
        timer = 0

@window.event
def on_key_press(s,m):
    global pos_z, rot_y
    if s == pyglet.window.key.W:
        pos[2] -= 5
    if s == pyglet.window.key.S:
        pos[2] += 5
    if s == pyglet.window.key.A:
        rot_y += 5
    if s == pyglet.window.key.D:
        rot_y -= 5


pyglet.clock.schedule(update)
pyglet.app.run()
