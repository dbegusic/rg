from pyglet.gl import *
from pyglet.window import key
from random import randint,randrange
import numpy as np

class Particle:
    def __init__(self,position,size,opacity,lifeTime):
        self.position = np.array(position)
        self.velocity = np.array([randrange(-3,3), randrange(-3,3), randrange(-3,3)])
        self.lifeTime = lifeTime
        self.opacity = opacity
        self.opacityORIGINAL = opacity
        self.size = size
        self.changeOfSize = 1
        self.colorChange = 0
        self.flag = randrange(0,100)

    def update(self, dt):
        self.position = self.position+self.velocity
        self.lifeTime -= 1
        if self.colorChange > 1.5:
            self.size = 0
        else:
            self.size += self.changeOfSize*2.5
        
        if self.opacity > 0:
            if self.opacity > self.opacityORIGINAL/2:
                self.opacity -= 1
                self.colorChange += 0.001953125
            elif self.opacity > self.opacityORIGINAL/4:
                self.opacity -= 0.75
                self.colorChange += 0.00390625
            elif self.opacity > self.opacityORIGINAL/8:
                self.opacity -= 0.5
                self.colorChange += 0.0078125
            elif self.opacity > self.opacityORIGINAL/16:
                self.opacity -= 0.25
                self.colorChange += 0.0078125
            elif self.opacity > self.opacityORIGINAL/32:
                self.opacity -= 0.125
                self.colorChange += 0.0078125
            elif self.opacity > self.opacityORIGINAL/64:
                self.opacity -= 0.0625
                self.colorChange += 0.0078125
            elif self.opacity > self.opacityORIGINAL/128:
                self.opacity -= 0.03125
                self.colorChange += 0.0078125
            else:
                self.opacity -= 0.015625
                self.colorChange += 0.0078125

class ParticleSystem:
    def __init__(self,texture,sourceX, sourceY,numberOfParticles=1):
        self.texture = texture
        self.sourceX = sourceX
        self.sourceY = sourceY
        self.particles = self.generateXRandomParticles(numberOfParticles)

    def generateXRandomParticles(self, x):
        tmp = randrange(-300,300)
        return [Particle([self.sourceX+tmp,self.sourceY+tmp,randrange(-100,100)],randrange(75,150),randrange(120,150),randrange(200,300)) for i in range(0,x)]

    def update(self, dt):
        for particle in self.particles:
            particle.update(dt)
            if particle.lifeTime <= 0:
                self.particles.remove(particle)
        if len(self.particles) == 0:
            self.particles = self.generateXRandomParticles(randint(50,100))

    def draw(self):
        glEnable(self.texture.target)
        glBindTexture(self.texture.target, self.texture.id)

        glEnable(GL_BLEND)
        glBlendFunc(GL_ONE, GL_ONE)
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA)

        glPushMatrix()
        for p in self.particles:
            matrix = (GLfloat * 16)()
            glGetFloatv(GL_MODELVIEW_MATRIX, matrix)
            matrix = list(matrix)
            CameraUp = np.array([matrix[1], matrix[5], matrix[9]])
            CameraRight = np.array([matrix[0], matrix[4], matrix[8]])

            v1 = p.position + CameraRight * p.size + CameraUp * -p.size
            v2 = p.position + CameraRight * p.size + CameraUp * p.size
            v3 = p.position + CameraRight * -p.size + CameraUp * -p.size
            v4 = p.position + CameraRight * -p.size + CameraUp * p.size

            glBegin(GL_QUADS)
            glTexCoord2f(0, 0)
            glVertex3f(v3[0], v3[1], v3[2])
            glTexCoord2f(1, 0)
            glVertex3f(v4[0], v4[1], v4[2])
            glTexCoord2f(1, 1)
            glVertex3f(v2[0], v2[1], v2[2])
            glTexCoord2f(0, 1)
            glVertex3f(v1[0], v1[1], v1[2])
            if p.flag>0 and p.flag<50: #dark red
                glColor4f(0.698, 0.340, 0.340, p.opacity/255);
            elif p.flag>50 and p.flag<80 and 1-p.colorChange>0: #yellowish to black
                glColor4f(1-p.colorChange, 0.5-p.colorChange, 0.0, p.opacity/255);
            elif p.flag>80 and p.flag<100: #yellowish white
                glColor4f(0.992, 1.0, 0.647, p.opacity/255);
            glEnd()

        glDisable(GL_BLEND)
        glPopMatrix()
        glDisable(self.texture.target)

class Window(pyglet.window.Window):
    global pyglet

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.source = [0, 0, 0]
        pyglet.clock.schedule_interval(self.update, 1.0 / 60.0)
        texture = pyglet.image.load('smoke.png').get_texture()
        self.pss = []
        for i in range(0,10):
            tmp = randrange(0,5)
            if tmp==0:
                texture = pyglet.image.load('smoke.png').get_texture()
            elif tmp==1:
                texture = pyglet.image.load('smoke2.png').get_texture()
            elif tmp>1:
                texture = pyglet.image.load('balloon.png').get_texture()
            self.pss.append(ParticleSystem(texture,randrange(-1500,1500),randrange(-1000,1000),75))

    def update(self, dt):
        [ps.update(dt) for ps in self.pss]
        
    def on_draw(self):
        self.clear()
        glClear(GL_COLOR_BUFFER_BIT)
        cameraPosition = [0, 0, 3000]
        lookAt = [0, 120, 0]

        glMatrixMode(GL_PROJECTION)
        glLoadIdentity()
        gluPerspective(50, self.width / self.height, 0.05, 10000)
        
        glMatrixMode(GL_MODELVIEW)
        glLoadIdentity()
        gluLookAt(cameraPosition[0],cameraPosition[1],cameraPosition[2],lookAt[0],lookAt[1],lookAt[2],0.0, 1.0, 0.0)
        
        for ps in self.pss:
            glPushMatrix()
            ps.draw()
            glPopMatrix()
        
        glFlush()

window = Window(width=800, height=640, caption="RG 2LAB", resizable=False)
keys = key.KeyStateHandler()
window.push_handlers(keys)
glClearColor(0.75, 0.75, 0.75, 1.0)
pyglet.app.run()
