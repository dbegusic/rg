# RG LAB 3
# Author: Domagoj Begusic

from pyglet.gl import *
from pyglet.window import key
from random import randrange

road = []
checkpointR = 0.0
barriers = []
checkpointB = -6.6
gameover = False
coins = []
checkpointC = -3.3
numCoins = 0
oldNumCoins = 0

class Coin:
    def __init__(self,ux,uy,uz):
        img = pyglet.image.load("data/images/coin.png")
        self.background = pyglet.sprite.Sprite(img)
        self.background.scale=1/16
        self.ux = ux+1
        self.uy = uy
        self.uz = uz
        self.rotateY = 0
        self.tick = 0
        pyglet.clock.schedule_interval(self.rotate, 1.0/240.0)
        
    def rotate(self,dt):
        if dt!=None:
            self.rotateY+=5
            if self.rotateY==360:
                self.rotateY=0

    def draw(self):
        global gameover
        if gameover:
            return
        
        self.rotate(None)
        glTranslated(self.ux,self.uy,self.uz)
        glPushMatrix()
        glPopMatrix()
        glTranslated(0.5,0.5,0)
        glRotatef(self.rotateY,0,1,0)
        glTranslated(-0.5,-0.5,0)
        glPushMatrix()
        
        self.background.draw()
        glPopMatrix()

class Block:

    def get_tex(self,file):
        tex = pyglet.image.load(file).get_texture()
        glTexParameterf(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_NEAREST)
        glTexParameterf(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_NEAREST)
        return pyglet.graphics.TextureGroup(tex)

    def __init__(self,ux,uy,uz,top="data/images/rtop.jpg",side="data/images/rtop.jpg",bottom="data/images/rtop.jpg",barrier=False):
        self.top = self.get_tex(top)
        self.side = self.get_tex(side)
        self.bottom = self.get_tex(bottom)

        self.batch = pyglet.graphics.Batch()

        tex_coords = ('t2f',(0,0, 1,0, 1,1, 0,1, ))

        self.ux = ux
        self.uy = uy
        self.uz = uz
                    
        x,y,z = ux,uy,uz
        X,Y,Z = x+1,y+1,z+1

        if barrier:
            self.batch.add(4,GL_QUADS,self.side,('v3f',(x,y,z, x,y,Z, x,Y,Z, x,Y,z, )),tex_coords)
            self.batch.add(4,GL_QUADS,self.side,('v3f',(X,y,Z, X,y,z, X,Y,z, X,Y,Z, )),tex_coords)
            self.batch.add(4,GL_QUADS,self.side,('v3f',(X,y,z, x,y,z, x,Y,z, X,Y,z, )),tex_coords)
            self.batch.add(4,GL_QUADS,self.side,('v3f',(x,y,Z, X,y,Z, X,Y,Z, x,Y,Z, )),tex_coords)
        #self.batch.add(4,GL_QUADS,self.bottom,('v3f',(x,y,z, X,y,z, X,y,Z, x,y,Z, )),tex_coords)
        self.batch.add(4,GL_QUADS,self.top,('v3f',(x,Y,Z, X,Y,Z, X,Y,z, x,Y,z, )),tex_coords)


    def draw(self):
        self.batch.draw()

flag = True
class Player:
    def __init__(self,pos=(0,0,0),rot=(0,0)):
        self.pos = list(pos)
        self.rot = list(rot)
        self.PRESSED = ""
        self.counter = 0
        self.speed = 0.15
        
        self.initSounds(None)
        
        self.movePlayer = pyglet.media.Player()
        self.movePlayer.queue(self.soundMove)
        
        self.coinPlayer = pyglet.media.Player()
        self.coinPlayer.queue(self.soundCoin)


        self.gamePlayer = pyglet.media.Player()
        self.gamePlayer.queue(self.soundGame)
        self.gamePlayer.play()

        pyglet.clock.schedule_interval(self.initSounds, 20)
    
    def initSounds(self,dt):
        self.soundMove = pyglet.media.load('data/sounds/move.mp3')
        self.soundCoin = pyglet.media.load('data/sounds/coin.mp3')
        self.soundGameOver = pyglet.media.load('data/sounds/gameover.wav')
        self.soundSpeedUp = pyglet.media.load('data/sounds/speedup.wav')
        self.soundGame = pyglet.media.load('data/sounds/game.mp3')
        #print("Sounds initialized!")

        
    def mouse_motion(self,dx,dy):
        dx/=8; dy/=8; self.rot[0]+=dy; self.rot[1]-=dx
        if self.rot[0]>90: self.rot[0] = 90
        elif self.rot[0]<-90: self.rot[0] = -90

    def update(self,dt,keys):
        self.gamePlayer.loop = True
        global gameover,numCoins,oldNumCoins,checkpointR,checkpointB,checkpointC,coins,barriers

        global flag
        if gameover and flag:
            self.gamePlayer = pyglet.media.Player()
            self.gamePlayer.queue(self.soundGame)
            self.gameoverPlayer = pyglet.media.Player()
            self.gameoverPlayer.queue(self.soundGameOver)
            self.gameoverPlayer.play()
            flag = False
        
        if keys[key.R]: #reset game, start over
            if gameover:
                #print(">>>Igra resetirana!")
                global window
                window.init()
                gameover = False
                numCoins = 0
                oldNumCoins = 0
                checkpointR = 0
                checkpointB = 6.6
                checkpointC = 3.3
                barriers = []
                coins = []
                flag = True
                self.gamePlayer.play()

        state = round(self.pos[0]/0.5)+1
        for b in barriers:
            if round(self.pos[2])!=b.uz:
                continue
            if b.ux == -1 and (state==0 or state==1):
                #print("Kolizija, lijevo!")
                gameover = True
                return
            if b.ux == 0 and (state==1 or state==2 or state==3):
                gameover = True
                #print("Kolizija, sredina!")
                return
            if b.ux == 1 and (state==3 or state==4):
                gameover = True
                #print("Kolizija, desno!")
                return
        
        for c in coins:
            if self.pos[2]<=c.uz+0.5 and self.pos[2]>=c.uz-0.5:
                if state/2-1==c.ux:
                    numCoins+=1
                    coins.remove(c)
                    #print("Skupljeno!","Coins: ",numCoins, "Coords: ",c.ux,c.uy,c.uz)
                    self.coinPlayer = pyglet.media.Player()
                    self.coinPlayer.queue(self.soundCoin)
                    self.coinPlayer.play()
                    break
        
        if numCoins!=oldNumCoins:
                if numCoins-oldNumCoins==10:
                    oldNumCoins = numCoins
                    self.speed+=0.0125
                    #print("Ubrzano! Brzina: ",self.speed)
                    self.speedupPlayer = pyglet.media.Player()
                    self.speedupPlayer.queue(self.soundSpeedUp)
                    self.speedupPlayer.play()
        self.pos[2]-=self.speed

 
        if self.counter == 10:
            self.counter = 0
            self.PRESSED = ""
        if self.counter < 10 and self.PRESSED=="LEFT":
            self.counter+=1
            if self.pos[0]>=-0.5:
                self.pos[0]-=0.1
        if self.counter < 10 and self.PRESSED=="RIGHT":
            self.counter+=1
            if self.pos[0]<=1.5:
                self.pos[0]+=0.1
        
        if self.PRESSED != "" and self.counter == 1:
            self.movePlayer = pyglet.media.Player()
            self.movePlayer.queue(self.soundMove)
            self.movePlayer.play()

        if keys[key.A]:
            if self.PRESSED == "":
                if self.pos[0]>=-0.5:
                    self.pos[0]-=0.1;
                    self.PRESSED = "LEFT"
        if keys[key.D]:
            if self.PRESSED == "":
                if self.pos[0]<=1.5:
                    self.pos[0]+=0.1;
                    self.PRESSED = "RIGHT"

        if keys[key.SPACE]: self.pos[1]+=1
        if keys[key.LSHIFT]: self.pos[1]-=1
        
        if self.pos[2]+checkpointR<=-10:
            global road
            road = road[(3*10):]
            lastZ = road[-1].uz
            for i in range(1,11):
                for x in [-1,0,1]:
                    road.append(Block(x,0,lastZ-i))
            #print(">>>Appended new road!")
            checkpointR += 10
        if self.pos[2]+checkpointB<=-10:
            for barrier in barriers:
                if self.pos[2]<barrier.uz:
                    barriers.remove(barrier)
            lastZ = road[-1].uz
            r1 = randrange(0,3)
            r2 = randrange(0,5)
            barriers.append(Block(r1-1,1,lastZ+r2,top="data/images/bside.jpg",side="data/images/bside.jpg",barrier=True))
            r1 = randrange(0,3)
            r2 = randrange(0,5)
            barriers.append(Block(r1-1,1,lastZ+r2,top="data/images/bside.jpg",side="data/images/bside.jpg",barrier=True))
            #print(">>>Appended new barriers!")
            checkpointB += 10

        if self.pos[2]+checkpointC<=-10:
            for coin in coins:
                if self.pos[2]<coin.uz:
                    coins.remove(coin)
                else:
                    break
            lastZ = road[-1].uz
            r1 = randrange(-1,2)
            coins.append(Coin(r1-1,1,lastZ-checkpointC))
            #print(">>>Appended new coin!")
            checkpointC += 10
        
class Window(pyglet.window.Window):

    def push(self,pos,rot): glPushMatrix(); glRotatef(-rot[0],1,0,0); glRotatef(-rot[1],0,1,0); glTranslatef(-pos[0],-pos[1],-pos[2],)
    def Projection(self): glMatrixMode(GL_PROJECTION); glLoadIdentity()
    def Block(self): glMatrixMode(GL_MODELVIEW); glLoadIdentity()
    def set2d(self): self.Projection(); gluOrtho2D(0,self.width,0,self.height); self.Block()
    def set3d(self): self.Projection(); gluPerspective(70,self.width/self.height,0.05,1000); self.Block()

    def setLock(self,state): self.lock = state; self.set_exclusive_mouse(state)
    lock = False; mouse_lock = property(lambda self:self.lock,setLock)

    def __init__(self,*args,**kwargs):
        super().__init__(*args,**kwargs)
        self.set_minimum_size(300,200)
        self.keys = key.KeyStateHandler()
        self.push_handlers(self.keys)
        pyglet.clock.schedule_interval(self.update,1.0/240.0)
        
        pyglet.clock.schedule_interval(self.drawBlocks,1.0/240.0)

        self.init()
        
        global numCoins
        self.label = pyglet.text.Label("Coins: "+str(numCoins),
                          font_name='Times New Roman',
                          font_size=30,
                          x=0, y=0,
                          anchor_x='left', anchor_y='bottom',color=(255,175,80,255))
        self.labelGameOver = pyglet.text.Label("Game Over",
                          font_name='Times New Roman',
                          font_size=30,
                          x=0, y=0,
                          anchor_x='left', anchor_y='bottom',color=(255,175,80,255))

    def init(self):
        global road
        width = 3
        length = 35
        for j in range(length):
            for i in range(width):
                road.append(Block(i-1,0,-j))

        self.player = Player((0.5,3.0,1.5),(-40,0))
        
        self.background = pyglet.sprite.Sprite(pyglet.image.load('data/images/background.jpg').get_texture())

    def drawBlocks(self,dt):
        global road
        if dt != None:
            self.clear()
            self.set3d()
            glPushMatrix()
            glTranslated(335, 210, -400)
            global numCoins
            self.label.text = str(numCoins)+" Coins"
            self.label.draw()
            glTranslated(-950, -300, -40)
            self.background.draw()
            glPopMatrix()
            self.push(self.player.pos,self.player.rot)
            for model in road:
                if self.player.pos[2]<model.uz+24 and self.player.pos[2]>model.uz:
                    model.draw()
            for barrier in barriers:
                if self.player.pos[2]<barrier.uz+24 and self.player.pos[2]>barrier.uz:
                    barrier.draw()
            for coin in coins:
                if self.player.pos[2]<coin.uz+24 and self.player.pos[2]>coin.uz:
                    coin.draw()
            global gameover
            if gameover:
                self.set2d()
                glTranslated(self.width/2-77,self.height/2,0)
                self.labelGameOver.draw()   
            glPopMatrix()
        
    def on_mouse_motion(self,x,y,dx,dy):
        if self.mouse_lock: self.player.mouse_motion(dx,dy)

    def on_key_press(self,KEY,MOD):
        if KEY == key.ESCAPE: self.close()
        elif KEY == key.E: self.mouse_lock = not self.mouse_lock

    def update(self,dt):
        self.player.update(dt,self.keys)

    def on_draw(self):
        self.drawBlocks(None)

global window
if __name__ == '__main__':
    window = Window(width=854,height=480,caption='Galactic Run',resizable=True)
    glEnable(GL_DEPTH_TEST)
    pyglet.app.run()



